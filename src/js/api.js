class Api {
  url = 'https://ajax.test-danit.com/api/v2/cards';
  tokenKey = 'stepProjectCardsToken';
  
  login(email = 'mail', password = 'password', callback) {
    fetch(`${this.url}/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({email,password})
    })
    .then(response => response.text())
    .then(token => {
      console.log('token -> ', token);
      localStorage.setItem(this.tokenKey,token);
      return token;
      // set token to local storage and maybe user name too
    })
    .catch(error => console.log('error', error)).then(callback);
  }

  getAllCards() {
    const token = localStorage.getItem(this.tokenKey);
    fetch(this.url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(response => console.log(response))
    .catch(error => console.log(error));
  }

  createCard(card) {
  const token = localStorage.getItem(this.tokenKey)
    fetch(this.url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(card)
    })
    .then(response => response.json())
    .then(response => console.log(response))
    .catch(error => console.log(error));
  }

  getCard(id) { 
  const token = localStorage.getItem(this.tokenKey);
  const url = `${this.url}/${id}`;
  fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(card => console.log(card))
    .catch(error => console.log(error));
  }

  editCard(id) {
    const token = localStorage.getItem(this.tokenKey);
    const url = `${this.url}/${id}`;
    fetch(url, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(card => console.log(card))
    .catch(error => console.log(error));
    console.log('call editCard -> ', id);
    // change
  }
  
  deleteCard(id) {
    const token = localStorage.getItem(this.tokenKey);
    const url = `${this.url}/${id}`;
    fetch(url, {
  method: 'DELETE',
  headers: {
    'Authorization': `Bearer ${token}`
  },
})
}
}


