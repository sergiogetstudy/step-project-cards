class Visit {
    constructor(goal, description, urgency, fullName) {
        this.goal = goal;
        this.description = description;
        this.urgency = urgency;
        this.fullName = fullName;

        this.elementInDOM = null;
    }

    renderField = (field,target,fieldClass) => {
        const newField = document.createElement('div');
        newField.classList.add(fieldClass); 
        newField.innerHTML =  field;
        target.append(newField);
    }

    renderVisit(target) {
        const cardWrapper = document.createElement("li");
        this.elementInDOM = cardWrapper;
        cardWrapper.classList.add("visit-card");
        target.append(cardWrapper);
        
        target.insertAdjacentHTML("beforeend", 
        `<svg class="modal__close-btn close-btn" width="15" height="15" viewBox="0 0 26 27" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M1 1L23.5 24" stroke="black" stroke-width="2" />
        <path d="M1 23L24 1" stroke="black" stroke-width="2" />
        </svg>`
        );

        this.renderField(`Пацієнт : ${this.fullName}`,
        this.elementInDOM, 'visit-card__field')

        console.log("rendered");
        // parent.insertAdjacentHTML('afterend', `
        // ${Function.makeCardiologistBlock(this.urgency, this.goal, this.description, this.diseases, this.bodyMassIndex, this.normalPressure, this.age, this.comments)}      
        //  `)
    }

}