const mediator = new Mediator();
const api = new Api();

class Main {
    constructor() {
    this.init();
    }

    init() {
        if(localStorage.getItem("stepProjectCardsToken") === null || localStorage.getItem("stepProjectCardsToken").includes("Validation failed") || localStorage.getItem("stepProjectCardsToken").includes("Incorrect username or password")) {
            document.querySelector(".js-header__login").addEventListener("click", () => {
                const modal = new Modal(".js-header__login");
                mediator.addEventListener('login', this.initDataForLoggedInUser);
            });
        } else {
            this.initDataForLoggedInUser();
        }
    }

    initDataForLoggedInUser = () => {
        document.querySelector(".js-header__login").remove();
        document.querySelector(".header__wrapper").insertAdjacentHTML("beforeend", 
        `<button class="js-header__new-visit-btn button header__button">ДОДАТИ ВІЗИТ</button>`);

        document.querySelector(".js-header__new-visit-btn").addEventListener("click", () => {
            console.log("test");
            const modal = new Modal(".js-header__new-visit-btn");
        });
    }
}

const main = new Main();
