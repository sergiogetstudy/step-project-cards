class Modal {
    constructor(auth) {
        this.auth = auth;
        this.render();
    }
    render() {
        
        const modalBackground = document.createElement("div");
        modalBackground.classList.add("modal");
        document.querySelector("body").prepend(modalBackground);
        
        const modalWindow = document.createElement("div");
        modalWindow.classList.add("modal__window");
        modalBackground.append(modalWindow);
        
        const modalHeadingWrap = document.createElement("div");
        modalHeadingWrap.classList.add("modal__heading-wrap");
        modalWindow.append(modalHeadingWrap);
        
        const modalHeading = document.createElement("h2");
        modalHeading.classList.add("modal__heading");
        modalHeadingWrap.append(modalHeading);
        
        modalHeadingWrap.insertAdjacentHTML("beforeend", 
        `<svg class="modal__close-btn close-btn" width="15" height="15" viewBox="0 0 26 27" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M1 1L23.5 24" stroke="black" stroke-width="2" />
        <path d="M1 23L24 1" stroke="black" stroke-width="2" />
        </svg>`
        );
        document.querySelector(".modal__close-btn").addEventListener("click", this.closeModalWindow);
        
        const renderSelector = (id, labelName, target, selectors) => {
            const newLabel = document.createElement("label");
            newLabel.classList.add("modal__label");
            newLabel.setAttribute("for", id);
            newLabel.innerHTML = labelName;
            target.append(newLabel);
            
            const newSelector = document.createElement("select");
            newSelector.classList.add("modal__input");
            newSelector.setAttribute("id", id);
            target.append(newSelector);
            
            selectors.forEach(selector => {
                const newOption = document.createElement("option");
                newSelector.setAttribute("value", selector);
                newOption.innerHTML = selector;
                newSelector.append(newOption);
            })
        }
        
            const renderInput = (id, labelName, inputType, target) => {
            const newLabel = document.createElement("label");
            newLabel.classList.add("modal__label");
            newLabel.setAttribute("for", id);
            newLabel.innerHTML = labelName;
            target.append(newLabel);
            
            const visitInput = document.createElement("input");
            visitInput.classList.add("modal__input");
            visitInput.setAttribute("id", id);
            visitInput.setAttribute("type", inputType);
            target.append(visitInput);
        }

        if(this.auth === ".js-header__login") {
            modalHeading.innerHTML = "Вхід до електронного кабінету";

            renderInput("input-login", "Введіть адресу електронної пошти:", "text", modalWindow);
            renderInput("input-password", "Введіть пароль:", "password", modalWindow);
    
        } else if(".js-header__new-visit-btn") {
            
            modalHeading.innerHTML = "Запис до лікаря";

            const doctorsArray = ["Кардіолог", "Дантист", "Терапевт"];

            renderSelector("select-doctor", "Оберіть лікаря:", modalWindow, doctorsArray);
         
            const visitFields = document.createElement("div");
            modalWindow.append(visitFields);

            const renderAditionalProperties = () => {
                visitFields.innerHTML = "";

                renderInput("visit-goal", "Мета візиту:", "text", visitFields);
                renderInput("visit-goal-desc", "Короткий опис візиту:", "text", visitFields);
                renderSelector("visit-priority-selector", "Оберіть терміновість:", visitFields, ["Звичайна", "Пріоритетна", "Невідкладна"]);
                renderInput("visit-patient-name", "Прізвище, ім'я, по-батькові пацієнта", "text", visitFields);
                switch(document.querySelector("#select-doctor").options[document.querySelector("#select-doctor").selectedIndex].innerHTML) {

                    case "Кардіолог":
                        renderInput("visit-carfiologist--preasure", "Звичайний артеріальний тиск:", "text", visitFields);
                        renderInput("visit-carfiologist--weight-index", "Індекс маси тіла:", "number", visitFields);
                        renderInput("visit-carfiologist--previous-diseases", "Перенесенні захворювання серцево-судинної системи:", "text", visitFields);
                        renderInput("visit-carfiologist--patient-age", "Вік:", "number", visitFields);
                        break;

                    case "Дантист":
                        renderInput("visit-carfiologist--last-visit-date", "Вік:", "date", visitFields);
                        break;

                    case "Терапевт":
                        renderInput("visit-carfiologist--patient-age", "Вік:", "number", visitFields);
                        break;
                }
            }
            renderAditionalProperties();

            document.querySelector("#select-doctor").addEventListener("click", renderAditionalProperties);
        }
        const modalSubmitBnt = document.createElement("input");
        modalSubmitBnt.setAttribute("type", "submit");
        modalSubmitBnt.classList.add("button");
        modalSubmitBnt.classList.add("modal__button");
        this.auth === ".js-header__login" ? modalSubmitBnt.value = "Надіслати" : modalSubmitBnt.value = "Створити візит";
        modalWindow.append(modalSubmitBnt);

        if(this.auth === ".js-header__login") {
            modalSubmitBnt.addEventListener("click", this.login.bind(this));
        } else {
            modalSubmitBnt.addEventListener("click", this.createVisit.bind(this));
        }

        const modalAnimation = () => modalBackground.classList.toggle("modal--active");
        setTimeout(modalAnimation, 0);
    }
    login() {
        const sucsess = () => {
            console.log(localStorage.getItem);
            if(localStorage.getItem("stepProjectCardsToken").includes("Validation failed") || localStorage.getItem("stepProjectCardsToken").includes("Incorrect username or password")) {
                return null;
            } else {
                const animationCloseModal = () => document.querySelector(".modal").remove();
                document.querySelector(".modal").classList.toggle("modal--active");
                setTimeout(animationCloseModal, 300);
                // this.auth === ".js-header__login" ? document.querySelector(".js-header__login").remove() : document.querySelector(".js-header__new-visit-btn").remove();

                mediator.publishEvent('login');
            }
        }
        const userMail = document.querySelector("#input-login").value;
        const userPassword = document.querySelector("#input-password").value;
        console.log(document.querySelector("#input-password").value);
        api.login(userMail, userPassword, sucsess);
        // api.login("sergiogetstudy@gmail.com", "vtkjvfy3250", sucsess);

    }
    createVisit() {
        console.log("added new visit");
        const goal = document.querySelector("#visit-goal").value;
        const description = document.querySelector("#visit-goal-desc").value;
        const urgency = document.querySelector("#visit-priority-selector").value;
        const fullName = document.querySelector("#visit-patient-name").value;
        const doctor = document.querySelector("#select-doctor").value;
        const normalPressure = document.querySelector("#visit-carfiologist--preasure")?.value;
        const bodyMassIndex = document.querySelector("#visit-carfiologist--weight-index")?.value;
        const diseases = document.querySelector("#visit-carfiologist--previous-diseases")?.value;
        const age = document.querySelector("#visit-carfiologist--patient-age")?.value;
        
        console.log(doctor);
        switch (doctor) {
            case "Кардіолог":
                new VisitCardiologist(goal, description, urgency, fullName, doctor, normalPressure, bodyMassIndex, diseases, age);
            break;
                
            case "Терапевт":
                new VisitTherapist(goal, description, urgency, fullName, doctor, age);
            break;

            case "Дантист":
            break;
        }
    }

    closeModalWindow() {
        const animationCloseModal = () => document.querySelector(".modal").remove();
        document.querySelector(".modal").classList.toggle("modal--active");
        setTimeout(animationCloseModal, 300);
        // mediator.removeEventListener('login', main.initDataForLoggedInUser);
    }
}