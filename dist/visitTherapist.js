      class VisitTherapist extends Visit {
        constructor(
            goal,
            description,
            urgency,
            fullName,
            doctor,
            age) {
            super(goal, description, urgency, fullName, doctor);
            this.goal = goal
            this.description = description
            this.urgency = urgency
            this.fullName = fullName
            this.doctor = doctor
            this.age = age

            this.renderToVisitCard(); 
        }

      renderToVisitCard() {

        this.renderVisit(document.querySelector(".main__visit-list"));
        this.renderField(`Лікар : ${this.doctor}`,this.elementInDOM,'visit-card__field');
        
        const cardShowMoBtn = document.createElement('button')
        cardShowMoBtn.innerHTML = 'Show More';
        this.elementInDOM.append(cardShowMoBtn);
        cardShowMoBtn.addEventListener('click',() => {
            const moreInfo =  this.elementInDOM.querySelector('.visit-card__more-info');
            moreInfo.classList.toggle('visit-card__more-info--hide');
        })
        
        const moreInfo = document.createElement("div");
        this.elementInDOM.append(moreInfo);
        moreInfo.classList.add('visit-card__more-info' ,'visit-card__more-info--hide')

        this.renderField( `Мета візиту : ${this.goal}` ,moreInfo , 'visit-card__field');
        this.renderField(`Вік : ${this.age}` ,moreInfo , 'visit-card__field');
        this.renderField(`Короткий опис візиту : 
        ${this.description}` ,moreInfo , 'visit-card__field'); 
    }
  }