class VisitCardiologist extends Visit {
    constructor(
            goal,
            description,
            urgency,
            fullName,
            doctor,
            normalPressure,
            bodyMassIndex,
            diseases,
            age
        ) {
        super(goal, description, urgency, fullName);

        this.doctor = doctor
        this.normalPressure = normalPressure
        this.bodyMassIndex = bodyMassIndex
        this.diseases = diseases
        this.age = age

        this.renderToVisitCard();
    }

    renderToVisitCard() {

        this.renderVisit(document.querySelector(".main__visit-list"));
        this.renderField(`Лікар : ${this.doctor}`,this.elementInDOM,'visit-card__field');
        
        const cardShowMoBtn = document.createElement('button')
        cardShowMoBtn.innerHTML = 'Show More';
        this.elementInDOM.append(cardShowMoBtn);
        cardShowMoBtn.addEventListener('click',() => {
            const moreInfo =  this.elementInDOM.querySelector('.visit-card__more-info');
            moreInfo.classList.toggle('visit-card__more-info--hide');
        })
        
        const moreInfo = document.createElement("div");
        this.elementInDOM.append(moreInfo);
        moreInfo.classList.add('visit-card__more-info' ,'visit-card__more-info--hide')

        this.renderField( `Мета візиту : ${this.goal}` ,moreInfo , 'visit-card__field');
        this.renderField(`Вік : ${this.age}` ,moreInfo , 'visit-card__field');
        this.renderField(`Короткий опис візиту : 
        ${this.description}` ,moreInfo , 'visit-card__field');
        this.renderField(`Терміновість :
        ${this.urgency}` ,moreInfo , 'visit-card__field');
        this.renderField( `Індекс маси тіла:
        ${this.bodyMassIndex}` ,moreInfo , 'visit-card__field');
        this.renderField( `Перенесенні захворювання серцево-судинної системи:
        ${this.diseases}` ,moreInfo , 'visit-card__field');
        this.renderField( `Звичайний артеріальний тиск:
        ${this.normalPressure}` ,moreInfo , 'visit-card__field'); 
        
    }
}

